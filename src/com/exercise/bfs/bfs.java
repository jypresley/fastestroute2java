/* Written by Presley Lacharmante (c) 2018 - exercise for Dev_backend Routing
 * 
 * Route Idea BFS - using a simple breath first method
 * https://en.wikipedia.org/wiki/Breadth-first_search
 * 
 * Future improvements : https://www.google.mu/search?q=algorithm+shortest+path+problem&oq=algorithm+shortest&aqs=chrome.5.0j69i57j69i61j0l3.17755j1j7&sourceid=chrome&ie=UTF-8
 * could have used Dijkstra's algorithm for recognised performance. Next time folks!
 * 
 * in this exercise, I will be using custom made bfs tree simple search
 * 
 * objective : input N computers, with each K amount of network interfaces
 *             assuming 2<=N<=90 and K<=5
 *             Time: 1000ms, Mem 64M (stack+heap)
 *             input example, 6 computers and test if route available or not + path
 *      
 */

package com.exercise.bfs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class bfs {
	
	static String filename = "input.txt";

	public static void main(String[] args) {

		System.out.println("Welcome to BFS exercise challenge Route (c) 2018 Presley Lacharmante\n");
		
		//capture start time
		long startTime = System.nanoTime();
		
		int N=0; //number of computers
		int K=0; //each computer's network cards
		int n=0; //computer count
		int node=0; //computer nodes
		int nodes=0; //total computer nodes
		
		int mode=0; //1:read next K, 2: reading list of K, 3:bye
		String[] positions = {};
		boolean hasRoute = false;
		
		//read file and put all in array list
		try {
			//System.out.println("Read file " + filename);
			File file = new File(filename);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			StringBuffer stringBuffer = new StringBuffer();
			String line;
			
			//defines array list of computers
			N = Integer.parseInt(bufferedReader.readLine()); //number of computers
			NetInterface myTwoDimensionalNetInterfaceArray[][] = new NetInterface[N][];
			n=0;
			
			while ((line = bufferedReader.readLine()) != null) {
				//stringBuffer.append(line);
				//stringBuffer.append("\n");

				if (node == K) {
					mode = 1;
				}
				
				//reading list of K - add to array
				if (mode == 2) {
					myTwoDimensionalNetInterfaceArray[n-1][node] = new NetInterface(line);
					node+=1; //increases node
					nodes+=1; //increases nodes
				}
				
				
				if (n == N) {
					mode = 3; //bye, reads now start-end path
					positions = line.split(" ");
				}
				
				//reading K
				if (mode == 1) {
					K = Integer.parseInt(line);
					
					//creates nodes to array
					myTwoDimensionalNetInterfaceArray[n] = new NetInterface[K];
					n += 1;
					mode = 2;
					node = 0;
				}
			}
			
			fileReader.close();
			//System.out.println("inputs are :");
			//System.out.println(stringBuffer.toString());
			
			//positions
			int start_position = Integer.parseInt(positions[0]);
			int stop_position = Integer.parseInt(positions[1]);
			
			//finding bfs path start to end
			String path = "";
			boolean hasPath = false;
			String prevItem ="";
			
			//injector
			int injector = start_position-1;
			
			for (int pointerNode = start_position-1; pointerNode < nodes; pointerNode ++) {
				path = "";
				
				//System.out.println("Injector PC=" + pointerNode);
				
				for (int x = pointerNode; x < stop_position; x ++) {
					NetInterface prev_subArray[] = myTwoDimensionalNetInterfaceArray[injector]; 
					NetInterface subArray[] = myTwoDimensionalNetInterfaceArray[x]; 
					
				    //System.out.println( "Length of array " + x + " is " + subArray.length );
				    
				    for (int y = 0; y < subArray.length; y ++) {
				    	NetInterface item = subArray[y];
			
				        //System.out.println(x + "  Item " + y + " is " + item.ip + "/" + item.subnet + " => " + item.mask + " => " + x + " " + y + " state=> " + item.state);
				        
				        //compare shifting
				        for (int i = y; i < prev_subArray.length; i++) {
				        	NetInterface prev_item = prev_subArray[i];
					        if (prev_item.SameSubnet(item)) {
					        	item.state = 2; //movable
					        	prev_item.state = 1; //crossed
					        	if (x == (stop_position-1)) {
					        		hasRoute = true; //route exists!
					        	}
					        } else {
					        	item.state = 3; //no route
					        }
				        }  
				    }
				}
				
				injector = pointerNode; //carried injector
			}
			
			//System.out.println("--- start final array / route ---- ");
			boolean cross= true;
			
			if (hasRoute) {
				System.out.println("Yes");
			} else {
				System.out.println("No route");
			}
			
			for (int x = 0; x < myTwoDimensionalNetInterfaceArray.length; x ++) {
				NetInterface subArray[] = myTwoDimensionalNetInterfaceArray[x]; 
				boolean firstNode = false;
			    //System.out.println( "Length of array " + x + " is " + subArray.length );
			    for (int y = 0; y < subArray.length; y ++) {
			    	NetInterface item = subArray[y];
			        //System.out.println( "  Item " + y + " is " + item.ip + " => " + x + " " + y + " state=> " + item.state);
			        
			        if (((item.state == 1 && cross == false) || ((item.state == 2) && x == (stop_position-1)) || (x == (start_position-1))) && (!firstNode)) {
			        	//jump display N node
			        	System.out.print((x+1) + " ");
			        	firstNode = true;
			        } 			        
			        
			        if (item.state == 1) {
			        	cross = true;
			        } else {
			        	cross = false;
			        }
			    }
			}
			
			System.out.println(""); //CRLF
			
			//System.out.println("--- end final array / route ---- ");
			
			long endTime = System.nanoTime();
			System.out.println("\nTime taken to run "+(endTime - startTime) / 1000000 + " ms"); 
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}
	

}


