/* Written by Presley Lacharmante (c) 2018 - exercise for Dev_backend Routing
 * Network Interface    
 */

package com.exercise.bfs;

public class NetInterface {
	    String ip;
	    String mask; 
	    String cidr;
	    int subnet;
	    int state; 
	    int parent;
	    
	    IPv4 ipv4Child;
	    
	    private String line;
	    
	    NetInterface(String line) {
	    	this.line = line;
  	
	    	String[] splitLine = line.split(" ");
	    	this.ip = splitLine[0];
	    	this.mask = splitLine[1];
	    	
	    	this.ipv4Child = new IPv4(this.ip,this.mask);
	    	this.cidr = ipv4Child.getCIDR();
	    	
	    	String[] splitCidr = cidr.split("/");
	    	this.subnet = Integer.parseInt(splitCidr[1]);

	    	this.state = 0; //0:blank 1:crossed 2:movable 3:closed
	    	this.parent = -1;
	    }
	    
	    public boolean SameSubnet(NetInterface targetIp) {
	    	//subnet range validation
	    	return this.ipv4Child.contains(targetIp.ip);
	    }
	       
}
